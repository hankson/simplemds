#ifndef MYLIBRARY_H
#define MYLIBRARY_H

#include <string>
#include <list>
#include "EnumDefine.h"

using std::string;

///"*****************************************************"///
/// Declarations
///"*****************************************************"///

string ToString( OrderStatus );
string ToString( ManufacturingOrderStatus );


///"*****************************************************"///
/// Definitions
///"*****************************************************"///

string ToString( OrderStatus status )
{
	string Status = "";
	switch( status )
	{
	case OrderFinished:
		{
			Status = "Finished";
			break;
		}
	case OrderDoing:
		{
			Status = "Doing";
			break;
		}
	case OrderUnworked:
		{
			Status = "Unworked";
			break;
		}
	case OrderError:
		{
			Status = "Error";
			break;
		}
	}

	return Status;
}

string ToString( ManufacturingOrderStatus status )
{
	string Status = "";
	switch( status )
	{
	case ManufacturingOrderFinished:
		{
			Status = "Finished";
			break;
		}
	case ManufacturingOrderDoing:
		{
			Status = "Doing";
			break;
		}
	case ManufacturingOrderUnworked:
		{
			Status = "Unworked";
			break;
		}
	case ManufacturingOrderError:
		{
			Status = "Error";
			break;
		}
	}

	return Status;
}

#endif