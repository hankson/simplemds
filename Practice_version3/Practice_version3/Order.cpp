#include "Order.h"

using namespace std;

Order::Order( int order_id , string name , int orderNumber , string date , int order_priority )
{
    id = order_id;
	itemName = name;
	number = orderNumber;
	deathLine = date;
	priority = order_priority;
	status = OrderUnworked;
	isDelay = false;
	needDays = 0;
	latestStartDate = "";
}

Order::~Order()
{
}

void Order::UpdateStatus( OrderStatus nowStatus )
{
	status = nowStatus;
}