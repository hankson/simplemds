//pch.h for vs2017 stdafx.h for vs2008//
//#include "pch.h"
#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <string>
#include <list>
#include <sstream>
#include <algorithm>
#include <time.h>
#include <map>
#include <math.h>
#include "MyLibrary.h"
#include "GeneralLibrary.h"
#include "Order.h"
#include "Item.h"

using namespace std;

///"*****************************************************"///
/// The paths of files
///"*****************************************************"///

string FilePath = "C:\\Users\\dgw\\Desktop\\11768\\Practice\\data\\";
string BomTableFileName = "";
string AltBomTableFileName = "";
string ExportFileName = "";
string InventoryFileName = "";
string CostFileName = "";
string OrderFileName = "";
string SpecificationFileName = "Specification.txt";
string LoopLogFileName = "LoopLog.txt";
string OrderFinishedLogFileName = "OrderFinishedLog.txt";

///"*****************************************************"///
/// Declaration
///"*****************************************************"///

class Item;
class ItemRatio;
class ManufacturingOrder;

list <Item*> ItemList;
list <Order*> OrderList;
list <ManufacturingOrder*> ManufacturingOrderList;

///"*****************************************************"///
/// Declaration Functions
///"*****************************************************"///

///View///
void ShowItemListData();
void ShowOrderListData();
void ShowManufacturingOrderListData();

///Moudle///

AddItemResult AddItemIntoTree( string , string , FileType , int , int );
void UpdateItemInventory( string , int );
void UpdateItemCost( string , int );

string TreeStructure( string itemName = "" );
string ItemStructure( Item* target , string loopLog = "" );
string ItemStructureChecker( Item* target , string queueValue = "" );
string ItemOneLayoutStructure( Item* target , int layout , string loopLog = "" );

OrderResult OrderItem( Order* order );
void AddOrder( string  );
void MasterDemandSchedules();
int ManufactureOrder( Item* , int , int );
float Orderschedule( int );

///Controller///
void OpenMenu();

///Import And Export///
void ImportSpecificationFile( string , string );
void ImportBomTableOrAltBomTable( string , string , FileType );
void ImportOrderTable( string, string );
void ImportInventoryTable( string, string );
void ImportCostTable( string, string );
void ExportFile( string , string );

///"*****************************************************"///
/// define class
///"*****************************************************"///

class ManufacturingOrder
{
public:
	int id;
	int order_id;
	Item* item;
	int number;
	ManufacturingOrderStatus status;

	ManufacturingOrder( int mo_id , int orderID , Item* itemData , int item_number )
	{
		id = mo_id;
		order_id = orderID;
		item = itemData;
		number = item_number;
		status = ManufacturingOrderUnworked;
	}

	void UpdateStatus( ManufacturingOrderStatus nowStatus )
	{
		status = nowStatus;
	}

	void UpdateNumber( int num )
	{
		number = num;

		if( number == 0 )
			status = ManufacturingOrderFinished;
		else
			status = ManufacturingOrderDoing;
	}
};

///"*****************************************************"///
/// View Functions
///"*****************************************************"///

void ShowItemListData()
{
	for ( list<Item*>::iterator iter = ItemList.begin(); iter != ItemList.end(); ++iter )
	{
		string show = (*iter)->itemName;
		show += "  PreviouslyItemS :";

		if ( !(*iter)->previouslyItemList.empty() )
		{
			for ( list<Item*>::iterator iter_node = (*iter)->previouslyItemList.begin(); iter_node != (*iter)->previouslyItemList.end(); ++iter_node )
			{
				show += " " + (*iter_node)->itemName;
			}
		}

		if ( !(*iter)->Ratio_NextItem_Map.empty() )
		{
			for ( map<ItemRatio*, Item*> ::iterator iterItem = (*iter)->Ratio_NextItem_Map.begin(); iterItem != (*iter)->Ratio_NextItem_Map.end(); ++iterItem )
			{
				show += (*iterItem).second->itemName;
				show += " Ratio : " + ToString( (*iterItem).first->inputRatio ) + " : " + ToString( (*iterItem).first->outputRatio ) + " |";
			}
		}

		show += "\nInventory : " + ToString( (*iter)->Inventory );
		show += " Need : " + ToString( (*iter)->costDays ) + " Day(s)";

		cout << show << endl;
	}

	cout << "*****************************************************" << endl;
}

void ShowOrderListData()
{
	string show = "";
	for ( list<Order*>::iterator iter = OrderList.begin(); iter != OrderList.end(); ++iter )
	{
		show += 
			"ID : " + ToString( (*iter)->id ) +
			" ItemName :" + (*iter)->itemName +
			" OrderNumber: " + ToString( (*iter)->number ) +
			" OrderNumber: " + (*iter)->deathLine +
			"\n" +
			"Status: " + ToString( (*iter)->status ) +
			" Priority: " + ToString( (*iter)->priority ) +
			" Delay: " + ToString( (*iter)->isDelay ) +
			" LatestStartDate: " + (*iter)->latestStartDate +
			"\n";
	}

	cout << show << endl;
	return;
}

void ShowManufacturingOrderListData()
{
	string show = "";
	for ( list<ManufacturingOrder*>::iterator iter = ManufacturingOrderList.begin(); iter != ManufacturingOrderList.end(); ++iter )
	{
		show += 
			"ID : " + ToString( (*iter)->id ) +
			" Order_ID : " + ToString( (*iter)->order_id ) +
			" ItemName :" + (*(*iter)->item).itemName +
			" number :" + ToString( (*iter)->number ) +
			" Status: " + ToString( (*iter)->status ) +
			"\n";
	}

	cout << show << endl;
	return;
}

///"*****************************************************"///
/// Modules Functions
///"*****************************************************"///

AddItemResult AddItemIntoTree( string output , string input , FileType type , int inputRatio , int outputRatio )
{
	if( output.empty() || input.empty() )
		return ItemDataEmpty;

	bool isNewTree = true;
	bool isExist = false;
	Item* newRootItem;
	Item* newLeafItem;
	
	if( !ItemList.empty() )
	{
		for ( list<Item*>::iterator iterItem = ItemList.begin(); iterItem != ItemList.end(); ++iterItem )
		{
			if( output.compare( (*iterItem)->itemName ) == 0 )
			{
				//check data isn't repeat
				for ( map<ItemRatio*, Item*> ::iterator iterNext = (*iterItem)->Ratio_NextItem_Map.begin(); iterNext != (*iterItem)->Ratio_NextItem_Map.end(); ++iterNext )
				{
					if( input.compare( (*iterNext).second->itemName ) == 0 )
						return ItemRepeat;
				}

				newLeafItem = (*iterItem);
				isNewTree = false;
			}

			if( input.compare( (*iterItem)->itemName ) == 0 )
			{
				isExist = true;
				newRootItem = (*iterItem);
			}
		}

		if( !isNewTree && isExist )
		{
			newLeafItem->AddNextItem( newRootItem , inputRatio , outputRatio );
			newRootItem->AddPreviouslyItem( newLeafItem );
		}
		else if( !isNewTree )
		{
			newRootItem = new Item( input , newLeafItem , NULL , type , inputRatio , outputRatio );
			newLeafItem->AddNextItem( newRootItem , inputRatio , outputRatio );
			ItemList.push_back( newRootItem );
		}
		else if( isExist )
		{
			newLeafItem = new Item( output , NULL , newRootItem , type , inputRatio , outputRatio );
			newRootItem->AddPreviouslyItem( newLeafItem );
			ItemList.push_back( newLeafItem );
		}
		else
		{
			newLeafItem = new Item( output , NULL , NULL , type , inputRatio , outputRatio );
			newRootItem = new Item( input , newLeafItem , NULL , type , inputRatio , outputRatio );
			newLeafItem->AddNextItem( newRootItem , inputRatio , outputRatio );
			ItemList.push_back( newLeafItem );
			ItemList.push_back( newRootItem );
		}
	}
	else
	{
		newLeafItem = new Item( output , NULL , NULL , type , inputRatio , outputRatio );
		newRootItem = new Item( input , newLeafItem , NULL , type , inputRatio , outputRatio );
		newLeafItem->AddNextItem( newRootItem , inputRatio , outputRatio );
		ItemList.push_back( newLeafItem );
		ItemList.push_back( newRootItem );
	}

	return AddItemSuccess;
}

void UpdateItemInventory( string itemName , int number )
{
	for ( list<Item*>::iterator iterItem = ItemList.begin(); iterItem != ItemList.end(); ++iterItem )
	{
		if( (*iterItem)->itemName == itemName )
		{
			(*iterItem)->Inventory = number;
		}
	}
	return;
}

void UpdateItemCost( string itemName , int costDays )
{
	for ( list<Item*>::iterator iterItem = ItemList.begin(); iterItem != ItemList.end(); ++iterItem )
	{
		if( (*iterItem)->itemName == itemName )
		{
			(*iterItem)->costDays = costDays;
		}
	}
	return;
}

string TreeStructure( string itemName )
{
	string result = "";

	if( itemName.empty() )
	{
		for ( list<Item*>::iterator iterItem = ItemList.begin(); iterItem != ItemList.end(); ++iterItem )
			if( (*iterItem)->previouslyItemList.empty() )
				result += ItemStructure( (*iterItem ) , ItemStructureChecker( (*iterItem) ) );
	}
	else
	{
		for ( list<Item*>::iterator iterItem = ItemList.begin(); iterItem != ItemList.end(); ++iterItem )
			if( (*iterItem)->itemName == itemName )
				result += ItemStructure( (*iterItem) , ItemStructureChecker( (*iterItem) ) );
	}

	return result;
}

string ItemStructure( Item* target , string loopLog )
{
	list<string> loopList;
	if( !loopLog.empty() )
	{
		stringstream ss( loopLog );
		string lineToken = "";
		while ( getline ( ss, lineToken , '\n') )
		{
			if( lineToken.empty() )
				continue;

			stringstream ssLine( lineToken );
			
			int length = ( lineToken.size() + 1 ) / 2;
			int count = 1;
			while (getline(ssLine, lineToken, ','))
			{
				if( count == (length -1) || count == length )
					loopList.push_back( lineToken );
				count++;
			}
		}
	}

	string result = "";
	list<Item*> needAddList;

	if( !target->Ratio_NextItem_Map.empty() )
	{
		list<Item*> itemList;
		for ( map<ItemRatio*, Item*> ::iterator iter = target->Ratio_NextItem_Map.begin(); iter != target->Ratio_NextItem_Map.end(); ++iter )
		{
			itemList.push_back( (*iter).second );
		}
		copy( itemList.begin() , itemList.end() , back_inserter( needAddList ) );
	}

	int layout = 0;
	result += ItemOneLayoutStructure( target , 0 );
	while( !needAddList.empty() )
	{
		layout++;
		list<Item*> nextLayoutList;

		for ( list<Item*>::iterator iter = needAddList.begin(); iter != needAddList.end(); ++iter )
		{
			list<Item*> itemList;
			for ( map<ItemRatio*, Item*> ::iterator iterItem = (*iter)->Ratio_NextItem_Map.begin(); iterItem != (*iter)->Ratio_NextItem_Map.end(); ++iterItem )
			{
				itemList.push_back( (*iterItem).second );
			}
			for ( list<Item*>::iterator iterNext = itemList.begin(); iterNext != itemList.end(); ++iterNext )
			{
				bool isLoop = false;
				if( !loopList.empty() )
				{
					for ( list<string>::iterator iterLoop = loopList.begin(); iterLoop != loopList.end(); ++iterLoop )
					{
						if( (*iter)->itemName.compare( (*iterLoop) ) == 0 )
						{
							iterLoop++;
							if( (*iterNext)->itemName.compare( (*iterLoop) ) == 0 )
								isLoop = true;
						}
						else
							iterLoop++;
					}
				}

				if( (*iterNext)->fileType == Bom && !isLoop )
					nextLayoutList.push_back( (*iterNext) );
			}

			if((*iter)->fileType == Bom)
				result.replace( result.find( (*iter)->itemName + "\n" ), string( (*iter)->itemName + "\n" ).size(), ItemOneLayoutStructure( (*iter) , layout , loopLog ) );

		}

		needAddList.clear();
		copy( nextLayoutList.begin() , nextLayoutList.end() , back_inserter( needAddList ) );
	}

	return result;
}

string ItemStructureChecker( Item* target , string queueValue )
{
	string loopLog = "";
	if( target->Ratio_NextItem_Map.empty() )
		return "";
	if( queueValue.empty() )
		queueValue = target->itemName;

	for ( map<ItemRatio*, Item*> ::iterator iter = target->Ratio_NextItem_Map.begin(); iter != target->Ratio_NextItem_Map.end(); ++iter )
	{
		if( (*iter).second->itemName == target->itemName )
		{
			loopLog += target->itemName + "," + (*iter).second->itemName + "\n" ;
		}
		else if( queueValue.find( (*iter).second->itemName ) != string::npos )
		{
			loopLog += queueValue + "," + (*iter).second->itemName + "\n" ;
		}
		else
		{
			loopLog += ItemStructureChecker( (*iter).second , queueValue + "," + (*iter).second->itemName );
		}
	}

	ExportFile( loopLog , LoopLogFileName );
	return loopLog;
}

string ItemOneLayoutStructure( Item* target , int layout , string loopLog )
{
	list<string> loopList;
	if( !loopLog.empty() )
	{
		stringstream ss( loopLog );
		string lineToken = "";
		while ( getline ( ss , lineToken, '\n') )
		{
			if( lineToken.empty() )
				continue;

			stringstream ssLine ( lineToken );
			
			int length = ( lineToken.size() + 1 ) / 2;
			int count = 1;
			while ( getline ( ssLine, lineToken , ',') )
			{
				if( count == (length -1) || count == length )
					loopList.push_back(lineToken);
				count++;
			}
		}
	}

	string result = target->itemName;
	if( target->fileType == AltBom )
		result += "(Alt)";
	result += "\n";

	if( target->Ratio_NextItem_Map.empty() )
		return result;

	string space = "";
	if( layout != 0 )
	{
		string blank = "   ";
		for( int i = 0 ; i < layout ; i ++ )
			space += blank;
	}

	for ( map<ItemRatio*, Item*> ::iterator iter = target->Ratio_NextItem_Map.begin(); iter != target->Ratio_NextItem_Map.end(); ++iter )
	{
		if( (*iter).second->fileType == AltBom )
			result.replace( result.find( target->itemName ), string( target->itemName ).size(), target->itemName + "+-" + (*iter).second->itemName + "(Alt)" );
		else
		{
			bool isLoop = false;
			for ( list<string>::iterator iterLoop = loopList.begin(); iterLoop != loopList.end(); ++iterLoop )
			{
				if( target->itemName.compare( (*iterLoop) ) == 0 )
				{
					iterLoop++;
					if( (*iter).second->itemName.compare( (*iterLoop) ) == 0 )
						isLoop = true;
				}
				else
					iterLoop++;
			}

			if( isLoop )
				result += space + "+-" + (*iter).second->itemName + "(Loop)" + "\n" ;
			else
				result += space + "+-" + (*iter).second->itemName + "\n" ;
		}
	}
	return result;
}

void AddOrder( string orderData )
{
	if( orderData.empty() )
		return;

	string itemName = "";
	int orderNumber = 0;
	string deadLine = "";
	int priority = 0;
	int OrderData_Specification_Length = 4;
	int Date_Specification_Length = 3;

	list<string> orderDataList = StringSplite( orderData , OrderData_Specification_Length );

	if( (int)orderDataList.size() < OrderData_Specification_Length )
		return;
	
	itemName = (*orderDataList.begin());
	orderDataList.pop_front();
	orderNumber = atoi( (*orderDataList.begin()).c_str() );
	orderDataList.pop_front();
	deadLine = (*orderDataList.begin());
	orderDataList.pop_front();
	priority = atoi( (*orderDataList.begin()).c_str() );

	if( itemName.empty() || orderNumber <= 0 || deadLine.empty() || priority <= 0 )
		return;

	orderDataList.clear();
	orderDataList = StringSplite( deadLine , Date_Specification_Length , '-' );

	if( (int)orderDataList.size() < Date_Specification_Length )
		return;

	int year = atoi( (*orderDataList.begin()).c_str() );
	orderDataList.pop_front();
	int month = atoi( (*orderDataList.begin()).c_str() );
	orderDataList.pop_front();
	int day = atoi( (*orderDataList.begin()).c_str() );

	if( year < 1900 || year > 2037 || month < 1 || month > 12 || day < 1 || day > 31)
		return;

	//id = 1 or last +1
	OrderList.push_back( new Order( OrderList.empty() ? 1 : ( * ( --OrderList.end() ) )->id + 1 , itemName , orderNumber , deadLine , priority ) );
}

void MasterDemandSchedules()
{
	for ( list<Order*>::iterator iter = OrderList.begin(); iter != OrderList.end(); ++iter )
	{
		if( (*iter)->status != OrderUnworked )
		{
			continue;
		}

		switch( OrderItem( (*iter) ) )
		{
		case OrderSuccess:
		case Insufficiency:
			{
				cout << "Success" << endl;
				break;
			}
		case ErrorInput:
			{
				cout << "Error input" << endl;
				break;
			}
		case OutOfItemList:
			{
				cout << "Item is not exist" << endl;
				break;
			}
		case OrderItemLoop:
			{
				cout << "The Bom of order item loop" << endl;
				break;
			}
		}
	}
}

OrderResult OrderItem( Order* order )
{
	int needNumber = 0;
	int needDay = 0;
	const int Date_Input_Length = 3;
	const int Date_Specification_Length = 11;

	stringstream ss( order->deathLine );
	string tokenS[ Date_Specification_Length ];
	int i = 0;
	string token = "";
	while ( getline ( ss , token , '-') )
	{
		tokenS[i] = token;
		++i;
		if( i == Date_Input_Length )
			break;
	}
	if( tokenS->length() < Date_Input_Length )
		return ErrorInput;

	int year = atoi( tokenS[0].c_str() );
	int month = atoi( tokenS[1].c_str() );
	int day = atoi( tokenS[2].c_str() );

	for ( list<Item*>::iterator iterItem = ItemList.begin(); iterItem != ItemList.end(); ++iterItem )
	{
		if( (*iterItem)->itemName == order->itemName )
		{
			struct tm date = { 0 , 0 , 0 , day , month - 1 , year - 1900 , 0 , 0 , 0 };
			
			if( order->number <= (*iterItem)->Inventory )
			{
				order->isDelay = mktime( &date ) < time( 0 );

				UpdateItemInventory( (*iterItem)->itemName , (*iterItem)->Inventory - order->number );
				order->UpdateStatus( OrderFinished );

				time_t t_date = mktime( &date );
				struct tm* t_latestStartDate = localtime( &t_date );
				char buffer[ Date_Specification_Length ];
				strftime( buffer , Date_Specification_Length , "%Y-%m-%d" , t_latestStartDate );
				string date( buffer );
				order->latestStartDate = date;
				return OrderSuccess;
			}
			else
			{
				needNumber = order->number - (*iterItem)->Inventory;
				(*iterItem)->Inventory = 0;

				//check AltBom
				for ( map<ItemRatio*, Item*> ::iterator iterAlt = (*iterItem)->Ratio_NextItem_Map.begin(); iterAlt != (*iterItem)->Ratio_NextItem_Map.end(); ++iterAlt )
				{
					if( (*iterAlt).second->fileType == AltBom )
					{
						if( needNumber < (*iterAlt).second->Inventory )
						{
							UpdateItemInventory( (*iterAlt).second->itemName , (*iterAlt).second->Inventory - needNumber );
							order->UpdateStatus( OrderFinished );
							time_t t_date = mktime( &date );
							struct tm* t_latestStartDate = localtime( &t_date );
							char buffer[ Date_Specification_Length ];
							strftime( buffer , Date_Specification_Length , "%Y-%m-%d" , t_latestStartDate );
							string date( buffer );
							order->latestStartDate = date;
							return OrderSuccess;
						}
						else
						{
							needNumber -= (*iterAlt).second->Inventory;
							UpdateItemInventory( (*iterAlt).second->itemName , 0 );
						}
					}
				}

				//id = 1 or last +1
				ManufacturingOrder* mo = new ManufacturingOrder( 
					ManufacturingOrderList.empty() ? 1 : ( * ( --ManufacturingOrderList.end() ) )->id + 1 ,
					order->id ,
					(*iterItem) ,
					needNumber );
				ManufacturingOrderList.push_back( mo );

				needDay = ManufactureOrder( (*iterItem) , needNumber , order->id );
				if( needDay < 0 )
					return OrderItemLoop;
				order->needDays = needDay;

				date.tm_mday -= needDay;
				time_t t_date = mktime( &date );

				order->isDelay = t_date < time( 0 );

				struct tm* t_latestStartDate = localtime( &t_date );

				char buffer[ Date_Specification_Length ];
				strftime( buffer , Date_Specification_Length , "%Y-%m-%d" , t_latestStartDate );
				string date( buffer );
				order->latestStartDate = date;

				order->UpdateStatus( OrderDoing );
				return Insufficiency;
			}
		}
	}
	order->UpdateStatus( OrderError );
	return OutOfItemList;
}

int ManufactureOrder( Item* item , int number , int order_id )
{
	int needDay = item->costDays * number ;
	int needNumber = 0;
	string loop = ItemStructureChecker( item );

	if( !loop.empty() )
	{
		cout << "loop : " << endl;
		cout << loop << endl;
		system("pause");
		return -1;
	}

	for ( map<ItemRatio*, Item*>::iterator iterItem = item->Ratio_NextItem_Map.begin(); iterItem != item->Ratio_NextItem_Map.end(); ++iterItem )
	{
		if( (*iterItem).second->fileType == AltBom )
			continue;

		needNumber = (int)ceil( (double)( number ) * (double)(*iterItem).first->outputRatio / (double)(*iterItem).first->inputRatio );
		
		//id = 1 or last +1
		ManufacturingOrder* mo = new ManufacturingOrder( 
			ManufacturingOrderList.empty() ? 1 : ( * ( --ManufacturingOrderList.end() ) )->id + 1 ,
			order_id ,
			(*iterItem).second ,
			needNumber );

		ManufacturingOrderList.push_back( mo );

		if( (*iterItem).second->Inventory < needNumber )
		{
			int makeNumber = needNumber - (*iterItem).second->Inventory;
			(*iterItem).second->Inventory = 0;
			needDay += ManufactureOrder( (*iterItem).second , makeNumber ,order_id );
			(*mo).UpdateNumber( makeNumber );
		}
		else
		{
			UpdateItemInventory( (*iterItem).second->itemName , (*iterItem).second->Inventory - needNumber );
			(*mo).UpdateNumber( 0 );
		}
	}

	return needDay;
}

float Orderschedule( int order_id )
{
	int allNumber = 0;
	int needDays = 0;
	int totalDays = 0;
	int costDays = 0;

	for ( list<Order*>::iterator iter_order = OrderList.begin(); iter_order != OrderList.end(); ++iter_order )
	{
		if( (*iter_order)->id == order_id )
		{
			if( (*iter_order)->status == OrderFinished )
			{
				return 100;
			}
			allNumber = (*iter_order)->number;

			for ( list<Item*>::iterator iter_item = ItemList.begin(); iter_item != ItemList.end(); ++iter_item )
			{
				if( (*iter_item )->itemName == (*iter_order)->itemName )
				{
					costDays = (*iter_item )->costDays;
					break;
				}
			}
		}
	}

	for ( list<ManufacturingOrder*>::iterator iter = ManufacturingOrderList.begin(); iter != ManufacturingOrderList.end(); ++iter )
	{
		if( order_id == (*iter)->order_id )
		{
			needDays += (*iter)->number * ( ( *(*iter)->item ).costDays );
			totalDays += allNumber * ( ( *(*iter)->item ).costDays );
		}
	}

	return ( needDays + costDays ) * 100 / ( totalDays + costDays );
}

///"*****************************************************"///
/// ImportAndExportFile functions
///"*****************************************************"///

void ImportSpecificationFile( string path , string fileName )
{
	int line = 0;
	ifstream fin;
	ifstream finTest;
	finTest.open( ( path + fileName ).c_str() );
	if( !finTest )
	{
		fin.open( ( fileName ).c_str() );
	}
	else
		fin.open( ( path + fileName ).c_str() );

	finTest.close();
	if ( !fin )
	{
		cout << fileName + " file is null" << endl;
		system("pause");
		return;
	}

	while ( fin.peek() != EOF )
	{
		string buffer = "";
		getline( fin , buffer );
		line++;
		if( !buffer.empty() )
		{
			stringstream ss( buffer );
			string tokenS[10];
			int i = 0;
			string token = "";
			while ( getline ( ss , token , ',') )
			{
				tokenS[i] = token;
				++i;
				if( i == 10 ) break;
			}

			switch(line)
			{
			case 1:
				{
					BomTableFileName = tokenS[0];
					break;
				}
			case 2:
				{
					AltBomTableFileName = tokenS[0];
					break;
				}
			case 3:
				{
					InventoryFileName = tokenS[0];
					break;
				}
			case 4:
				{
					CostFileName = tokenS[0];
					break;
				}
			case 5:
				{
					OrderFileName = tokenS[0];
					break;
				}
			}
		}
	}
	fin.close();

	return;
}

void ImportBomTableOrAltBomTable( string path , string fileName , FileType type )
{
	int line = 0;
	ifstream fin;
	ifstream finTest;
	finTest.open( ( path + fileName ).c_str() );
	if( !finTest )
	{
		fin.open( ( fileName ).c_str() );
	}
	else
		fin.open( ( path + fileName ).c_str() );

	finTest.close();
	if ( !fin )
	{
		cout << fileName + " file is null" << endl;
		system("pause");
		return;
	}

	while( fin.peek() != EOF )
	{
		string buffer = "";
		getline( fin , buffer );
		line++;

		if ( !buffer.empty() && buffer.find("output_part_id") == string::npos && buffer.find("input_part_id") == string::npos )
		{
			stringstream ss( buffer );
			string tokenS[4];
			int i = 0;
			string token = "";
			while( getline ( ss, token , ',') )
			{
				tokenS[i] = token;
				++i;
			}
			
			switch( AddItemIntoTree ( tokenS[0] , tokenS[1] , type , atoi( tokenS[2].c_str() ) , atoi( tokenS[3].c_str() ) ) )
			{
				case ItemRepeat:
					{
						cout << "Line " + ToString(line) + " : " + buffer + " is repeeat." << endl;
						break;
					}
				case ItemDataEmpty:
					{
						cout << "Line " + ToString(line) + " : " + buffer + " is empty." << endl;
						break;
					}
				case AddItemSuccess:
					{
						//create success log?
						break;
					}
			}
		}
	}
	fin.close();
}

void ImportOrderTable( string path, string fileName )
{
	ifstream fin;
	ifstream finTest;
	finTest.open( ( path + fileName ).c_str() );
	if( !finTest )
	{
		fin.open( ( fileName ).c_str() );
	}
	else
		fin.open( ( path + fileName ).c_str() );

	finTest.close();
	if ( !fin )
	{
		cout << fileName + " file is null" << endl;
		system("pause");
		return;
	}

	while (fin.peek() != EOF)
	{
		string buffer = "";
		getline( fin , buffer );

		if ( !buffer.empty() )
		{
			AddOrder( buffer );
		}
	}
	fin.close();
	return;
}

void ImportInventoryTable( string path, string fileName )
{
	string itemName = "";
	int number = 0;

	ifstream fin;
	ifstream finTest;
	finTest.open( ( path + fileName ).c_str() );
	if( !finTest )
	{
		fin.open( ( fileName ).c_str() );
	}
	else
		fin.open( ( path + fileName ).c_str() );

	finTest.close();
	if ( !fin )
	{
		cout << fileName + " file is null" << endl;
		system("pause");
		return;
	}

	while( fin.peek() != EOF )
	{
		string buffer = "";
		getline( fin , buffer );

		if ( !buffer.empty() )
		{
			stringstream ss( buffer );
			string tokenS[2];
			int i = 0;
			string token;
			while( getline ( ss, token , ',') )
			{
				tokenS[i] = token;
				++i;
			}
			itemName = tokenS[0];
			number = atoi( tokenS[1].c_str() );
		}
		UpdateItemInventory( itemName , number );
	}
	fin.close();
	return;
}

void ImportCostTable( string path, string fileName )
{
	string itemName = "";
	int costDays = 0;

	ifstream fin;
	ifstream finTest;
	finTest.open( ( path + fileName ).c_str() );
	if( !finTest )
	{
		fin.open( ( fileName ).c_str() );
	}
	else
		fin.open( ( path + fileName ).c_str() );

	finTest.close();
	if( !fin )
	{
		cout << fileName + " file is null" << endl;
		system("pause");
		return;
	}

	while( fin.peek() != EOF )
	{
		string buffer = "";
		getline( fin , buffer );

		if (!buffer.empty())
		{
			stringstream ss( buffer );
			string tokenS[2];
			int i = 0;
			string token;
			while ( getline ( ss , token , ',') )
			{
				tokenS[i] = token;
				++i;
				if( i == 2 ) break;
			}
			itemName = tokenS[0];
			costDays = atoi( tokenS[1].c_str() );
		}
		UpdateItemCost( itemName,costDays );
	}
	fin.close();
	return;
}

void ExportFile( string result , string fileName )
{
	ofstream fout;
	fout.open( ( FilePath + fileName ).c_str(), ios::out );

	if ( !fout )
	{
		ofstream newfile( fileName.c_str() );
		if (newfile)
		{
			newfile << result << endl;
		}
		newfile.close();
	}
	else
	{
		fout << result << endl;
	}
	fout.close();
}

///"*****************************************************"///
/// Controller Functions
///"*****************************************************"///

void OpenMenu()
{
	int choose = 1;

	while(choose != 0)
	{
		system("cls");
		cout << "Welcome to use SimpleMDS system.You can :" << endl;
		cout << "1.Show all Item data." << endl;
		cout << "2.Show the tree structure." << endl;
		cout << "3.Search item in tree." << endl;
		cout << "4.Export tree structure to export file." << endl;
		cout << "5.Order Item." << endl;
		cout << "6.Show all Order data." << endl;
		cout << "7.Show all Manufacturing_Order data." << endl;
		cout << "8.Search schedule of the order" << endl;
		cout << "9.Master Demand Schedules" << endl;
		cout << "0.Exit." << endl;
		cout << endl;
		cin.clear();
		cin >> choose;
		cin.sync();
		system("cls");

		switch(choose)
		{
		case 0:
			{
				return;
			}
		case 1:
			{
				ShowItemListData();
				break;
			}
		case 2:
			{
				string result = TreeStructure();
				cout << result << endl;
				break;
			}
		case 3:
			{
				cout << "What Item do you want to search?" << endl;
				string itemName;
				cin >> itemName;
				cin.sync();
				system("cls");
				cout << TreeStructure( itemName ) << endl;
				break;
			}
		case 4:
			{
				ExportFile( TreeStructure() , ExportFileName );
				cout << "The tree data is exported" << endl;
				break;
			}
		case 5:
			{
				cout << "Order what you wanna , number , date and priority" << endl;
				cout << "Input Example : A,10,2017-11-11,10" << endl;
				string input;
				cin >> input;
				cin.sync();
				system("cls");
				AddOrder( input );
				break;
			}
		case 6:
			{
				ShowOrderListData();
				break;
			}
		case 7:
			{
				ShowManufacturingOrderListData();
				break;
			}
		case 8:
			{
				cout << "Whitch order do you wanna check schedule?" << endl;
				int input;
				cin >> input;
				cin.sync();
				system("cls");
				cout << Orderschedule( input );
				cout << "%" << endl;
				break;
			}
		case 9:
			{
				MasterDemandSchedules();
				break;
			}
		default:
			{
				cout << "OrderError input" << endl;
				break;
			}
		}
		system("pause");
	}
}

///"*****************************************************"///
/// Main
///"*****************************************************"///

int main()
{
	//Import file data
	ImportSpecificationFile( FilePath , SpecificationFileName );
	ImportBomTableOrAltBomTable( FilePath , BomTableFileName , Bom );
	ImportBomTableOrAltBomTable( FilePath , AltBomTableFileName , AltBom );
	ImportInventoryTable( FilePath,InventoryFileName );
	ImportCostTable( FilePath,CostFileName );
	ImportOrderTable( FilePath,OrderFileName );
	MasterDemandSchedules();

	OpenMenu();

	return 0;
}
