#ifndef ENUMDEFINE_H
#define ENUMDEFINE_H

///"*****************************************************"///
/// Define Enum
///"*****************************************************"///

enum FileType
{
	Bom,
	AltBom
};

enum AddItemResult
{
	AddItemSuccess,
	ItemDataEmpty,
	ItemRepeat
};

enum OrderResult
{
	OrderSuccess,
	Insufficiency,
	ErrorInput,
	OutOfItemList,
	OrderItemLoop
};

enum OrderStatus
{
	OrderFinished,
	OrderDoing,
	OrderUnworked,
	OrderError
};

enum ManufacturingOrderStatus
{
	ManufacturingOrderFinished,
	ManufacturingOrderDoing,
	ManufacturingOrderUnworked,
	ManufacturingOrderError
};

#endif