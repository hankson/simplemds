#include "Item.h"

using namespace std;

Item::Item( string data , Item* preItem , Item* nextItem , FileType type ,int inputRatio , int outputRatio )
{
	itemName = data;
	if( preItem != NULL ) previouslyItemList.push_back( preItem );
	fileType = type;
	if ( nextItem != NULL ) Ratio_NextItem_Map.insert( pair< ItemRatio* , Item* >( ( new ItemRatio( inputRatio , outputRatio ) ) , nextItem ) );
	Inventory = 0;
	costDays = 0;
}

void Item::AddNextItem( Item* nextItem ,int inputRatio , int outputRatio )
{
	if ( nextItem != NULL ) Ratio_NextItem_Map.insert( pair< ItemRatio* , Item* >( ( new ItemRatio( inputRatio , outputRatio ) ) , nextItem ) );
}

void Item::AddPreviouslyItem( Item* preItem )
{
	if( preItem != NULL ) previouslyItemList.push_back( preItem );
}

ItemRatio::ItemRatio( int input , int output )
{
	inputRatio = input;
	outputRatio = output;
}