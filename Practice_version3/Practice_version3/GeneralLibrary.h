#ifndef GENERALLIBRARY_H
#define GENERALLIBRARY_H

#include <string>
#include <list>

using std::string;
using std::stringstream;
using std::list;

///"*****************************************************"///
/// Declarations
///"*****************************************************"///

string ToString( int );
string ToString( bool judgement );
list< string > StringSplite( string data , int length , char symbol = ',');

///"*****************************************************"///
/// Definitions
///"*****************************************************"///

string ToString( int number )
{
	stringstream ss;
	ss << number;
	return ss.str();
}

string ToString( bool judgement )
{
	string message = "false";
	if( judgement )
		message = "true";
	return message;
}

list< string > StringSplite( string data , int length , char symbol )
{
	list< string > resultList;

	stringstream ss( data );
	int i = 0;
	string token = "";
	while ( getline ( ss , token , symbol ) )
	{
		resultList.push_back( token );
		++i;
		if( i == length ) break;
	}

	return resultList;
}

#endif