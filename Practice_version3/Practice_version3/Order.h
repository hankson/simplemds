#ifndef ORDER_H
#define ORDER_H

#include <string>
#include "EnumDefine.h"

using std::string;

class Order
{ 
public: 
	int id;
	string itemName;
	int number;
	string deathLine;
	int priority;
	OrderStatus status;
	bool isDelay;
	int needDays;
	string latestStartDate;

    Order( int order_id , string name , int orderNumber , string date , int order_priority );
     ~Order();
    void UpdateStatus( OrderStatus nowStatus );
};

#endif