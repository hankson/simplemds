#ifndef ITEM_H
#define ITEM_H

#include <string>
#include <map>
#include <list>
#include "EnumDefine.h"

using std::string;
using std::map;
using std::list;

class ItemRatio
{
public:
	int inputRatio;
	int outputRatio;

	ItemRatio( int input , int output );
};

class Item
{
public:
	string itemName;
	list <Item*> previouslyItemList;
	map < ItemRatio* , Item* > Ratio_NextItem_Map;
	FileType fileType;
	int Inventory;
	int costDays;

	Item( string data , Item* preItem , Item* nextItem , FileType type ,int inputRatio , int outputRatio );
	void AddNextItem( Item* nextItem ,int inputRatio , int outputRatio );
	void AddPreviouslyItem( Item* preItem );
};

#endif